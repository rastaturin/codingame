package my.projects.CGX;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * This is solution for a puzzle "CGX Formatter" from Codingame.
 */
class Solution {

    public static void main(String args[]) {

        Scanner in = new Scanner(System.in);

        int N = in.nextInt();
        in.nextLine();

        Structure str = new Structure();

        for (int i = 0; i < N; i++) {
            String CGXLine = in.nextLine();
            str.addLine(CGXLine);
        }

        str.parse();
        str.print();
    }
}

/**
 * Parses lines into Elements
 */
class Structure {
    Element el;
    String src = "";

    public void addLine(String line) {
        src += line;
    }

    public void parse() {
        el = createEl(src);
    }

    public void print() {
        el.print(0, false);
    }

    public static void sprint(String str, int indent) {
        while (indent-- > 0) System.out.print(" ");
        System.out.print(str);
    }

    public static void sprintln(String str, int indent) {
        sprint(str, indent);
        System.out.println();
    }

    /**
     * Creates element based on string
     * @param str
     * @return
     */
    protected static Element createEl(String str) {
        str = str.trim();
        if (str.startsWith("(")) {
            int end = str.lastIndexOf(")");
            return new Block(str.substring(str.indexOf("(") + 1, end).trim());
        }
        if (str.startsWith("'") && str.indexOf("'", str.indexOf("'") + 1) > -1 && str.indexOf("=", str.indexOf("'", str.indexOf("'") + 1)) > -1) {
            return new Keyval(str);
        } else {
            return new Primitive(str);
        }
    }


}

/**
 * Primitive element
 */
class Primitive extends Element {
    public int type;
    public String string;
    public int val;

    public void print(int indent, boolean addSep) {
        Structure.sprint(string, indent);
        if (addSep) {
            Structure.sprint(";", 0);
        }
        Structure.sprintln("", 0);
    }

    public Primitive(String str) {
        string = str;
    }
}

/**
 * Block element
 */
class Block extends Element {
    ArrayList<Element> elements;

    public void print(int indent, boolean addSep) {
        Structure.sprintln("(", indent);
        for (int i = 0; i < elements.size(); i++) {
            elements.get(i).print(indent + 4, i < elements.size() - 1);
        }
        Structure.sprintln(")" + (addSep ? ";" : ""), indent);
    }

    /**
     * Parse string
     */
    public Block(String s) {
        String out = "";
        int openBr = 0;
        elements = new ArrayList<Element>();

        while (s.length() > 0) {
            if (openBr == 0 && s.startsWith(";")) {
                elements.add(Structure.createEl(out));
                out = "";
            } else {
                out += s.substring(0, 1);
            }

            if (s.startsWith("(")) {
                openBr++;
            }
            if (s.startsWith(")")) {
                openBr--;
            }
            s = s.substring(1);
        }

        if (out.trim().length() > 0) {
            elements.add(Structure.createEl(out));
        }

    }
}

/**
 * Key-val element
 */
class Keyval extends Element {
    public String name;
    protected Element elemetn;

    public void print(int indent, boolean addSep) {
        if (elemetn instanceof Primitive) {
            Structure.sprint(name + "=", indent);
            elemetn.print(0, addSep);
        } else {
            Structure.sprintln(name + "=", indent);
            elemetn.print(indent, addSep);
        }
    }

    public Keyval(String str) {
        name = str.substring(0, str.indexOf("=")).trim();
        String val = str.substring(str.indexOf("=") + 1).trim();
        elemetn = Structure.createEl(val);
    }
}

/*
 * Abstract element
 */
abstract class Element {
    abstract public void print(int indent, boolean addSep);
}
