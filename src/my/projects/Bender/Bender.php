<?php
/**
*   This is solution for puzzle "Bender, a depressed robot" at codingame.com
*   http://www.codingame.com/ide/10657390854b1b308aafd14058c93c4ec9e3f73
*/

$map = array();

fscanf(STDIN, "%d %d",
    $L,
    $C
);

// Reading map

for ($i = 0; $i < $L; $i++) {
    $row = stream_get_line(STDIN, $C+1, "\n");
    $r = str_split($row);
    for ($j = 0; $j < $C; $j++) {
        $map[$j][$i] = $r[$j];
        if ($r[$j] == "@") {
            $x = $j; $y = $i;
        }
    }
}

// Create Bender and move

$bender = new Bender($x, $y, $map);
$bender->move();

/**
*  Bender
*/
class Bender
{
    public $x, $y;
    public $breaking = false;
    public $inverted = false;
    public $direction = 'SOUTH';
    public $map;
    public $path = "";

    public $swd = array(
        false => array(
            'SOUTH',
            'EAST',
            'NORTH',
            'WEST',
            ),
        true => array(
            'WEST',
            'NORTH',
            'EAST',
            'SOUTH',
            ),
        );

    public function __construct($x, $y, &$map)
    {
        $this->x = $x;
        $this->y = $y;
        $this->map = $map;
    }

    public function move(){
        if (strlen($this->path)>100000) {
            echo "LOOP\n";exit();
        }
        $i = 0;
        while (!$this->canStep())
        {
            $this->direction = $this->swd[$this->inverted][$i];
            $i++;
        }
        $this->path .= "$this->direction\n";
        $this->step();
        $this->changedirection();
        $this->move();
    }

    protected function step()
    {
        switch ($this->direction) {
            case "SOUTH": $this->y++; break;
            case "NORTH": $this->y--; break;
            case "EAST": $this->x++; break;
            case "WEST": $this->x--; break;
        }
    }

    protected function changedirection()
    {
        switch ($this->map[$this->x][$this->y])
        {
            case "$": echo $this->path; exit();break;
            case "S": $this->direction = "SOUTH"; break;
            case "W": $this->direction = "WEST"; break;
            case "N": $this->direction = "NORTH"; break;
            case "E": $this->direction = "EAST"; break;
            case "I": $this->inverted = !$this->inverted; break;
            case "B": $this->breaking = !$this->breaking; break;
            case "T": list($this->x, $this->y) = $this->getOtherT($this->x, $this->y); break;
            case "X": $this->map[$this->x][$this->y] = " ";break;
        }


    }

    /** Fidn other teleport */
    protected function getOtherT($x, $y)
    {
        foreach ($this->map as $i=>$line)
        {
            foreach ($line as $j=>$c)
            {
                // error_log(var_export("$c at $i $j!", true));
                if ($c == "T" && ($i != $x || $j!=$y))
                {
                    error_log(var_export("Teleport from $x $y to $i $j!", true));
                    return array($i, $j);
                }
            }
        }
    }

    protected function chDirection()
    {
        switch ($this->direction)
        {
            case "SOUTH": $this->y++; break;
            case "NORTH": $this->y--; break;
            case "EAST": $this->x++; break;
            case "WEST": $this->x--; break;
        }
    }

    protected function m()
    {
        return $this->map[$this->x][$this->y];
    }

    protected function canStep()
    {
        $x = $this->x;
        $y = $this->y;
        $this->step();
        $res = ($this->m()!="#" && ($this->m()!="X" || $this->breaking));
        $this->x = $x;
        $this->y = $y;
        return $res;
    }
}