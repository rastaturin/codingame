<?php
/**
 * This is a puzzle "Labyrinth" from http://www.codingame.com
 * See Labyrinth.txt
 * Replay: http://www.codingame.com/replay/solo/28311793?fb_ref=Default
 **/

fscanf(STDIN, "%d %d %d",
    $R, // number of rows.
    $C, // number of columns.
    $A // number of rounds between the time the alarm countdown is activated and the time the alarm goes off.
);

$k = new Kirk($C, $R, $A);

// game loop
while (TRUE)
{
    fscanf(STDIN, "%d %d",
        $KR, // row where Kirk is located.
        $KC // column where Kirk is located.
    );
    $k->setXY($KC, $KR);

    for ($i = 0; $i < $R; $i++)
    {
        fscanf(STDIN, "%s",
            $ROW // C of the characters in '#.TC?' (i.e. one line of the ASCII maze).
        );
        $k->lab->setLine($i, $ROW);

    }

    $k->step();
}


class Kirk
{
    public $lab;
    /** Point @var Point*/
    private $current;
    private $visited = false;
    /** Target @var Point*/
    private $target = null;
    private $alarm;

    public function setXY($x, $y)
    {
        $this->current = new Point($x, $y);
    }

    public function __construct($w, $h, $alarm)
    {
        $this->lab = new Labirint($w, $h);
        $this->alarm = $alarm;
    }

    /**
     * Next step
     * @return Point
     */
    private function findNext()
    {
        if ($this->visited)
        {
            return $this->goBack();
        }

        if (($toC = $this->countToC()) && $this->lab->fromCtoT() <= $this->alarm)
        {
            if ($toC == 1)
            {
                $this->visited = true;
            }
            $this->countToC();
            return $this->stepOnPath();
        } else {
            return $this->explore();
        }
    }

    private function countToC()
    {
        if ($this->lab->pointC && ($toC = $this->countStepsTo($this->lab->pointC))>-1)
        {
            return $toC;
        }
    }

    private function getTarget()
    {
        if (!$this->target || $this->current->nearTo($this->target) || $this->lab->pointC && $this->current->nearTo($this->lab->pointC))
        {
            $this->target = $this->findTarget();
        }
        return $this->target;
    }

    private function findTarget()
    {
        $q = $this->lab->getNearestQ($this->current);
        if (!$q) {
            $q = $this->lab->randNeib($this->current);
        }
        return $q;
    }

    private function explore()
    {
        $this->countStepsTo($this->getTarget());
        return $this->stepOnPath();
    }

    private function goBack()
    {
        $this->countStepsTo($this->lab->pointT);
        return $this->stepOnPath();
    }

    private function countStepsTo(Point $p)
    {
        return $this->lab->findPath($this->current, $p);
    }

    private function stepOnPath()
    {
        return $this->lab->stepOnPath($this->current);
    }

    public function step()
    {
        $next = $this->findNext();

        echo $this->current->go($next)."\n";
    }

}


class Labirint {

    public $data = array();
    private $d1 = array();
    private $queue = array();

    public $pointC = null;
    public $pointT = null;

    public function fromCtoT()
    {
        return $this->findPath($this->pointC, $this->pointT);
    }

    public function randNeib($p)
    {
        $nr = $this->getNearestToStep($p);
        return  $nr[array_rand($nr)];
    }

    /**
     * Find nearest invisible point
     * @param Point $p
     */
    public function getNearestQ(Point $p)
    {
        $this->resetD1();
        $this->queue = array();
        return $this->findQ($p);
    }

    /**
     * Nearest invisible
     * @param Point $p
     * @return Point
     */
    private function findQ(Point $p)
    {
        if ($q = $this->getNeibQ($p)) {
            return $q;
        }

        $this->setD1($p, 1);
        foreach ($this->getNearestToStep($p) as $np)
        {
            if ($this->getD1($np)!= 1 && !in_array($np, $this->queue) && $np != $this->pointC) {
                //$np->show();
                array_push($this->queue, $np);
            }
        }

        if (count($this->queue)) {
            if ($q = $this->findQ(array_shift($this->queue)))
            {
                return $q;
            }
        }
    }


    /**
     * Distance to $to
     * @param Point $from
     * @param Point $to
     * @return int
     */
    public function findPath(Point $from, Point $to)
    {
        if (!$to) return -1;
        $this->resetD1();
        $this->queue = array();
        $this->fillCell($to);
        return $this->getD1($from);
    }

    /**
     * Step to visible point
     * Use findPath first
     */
    public function stepOnPath(Point $from)
    {
        $p = $from;
        foreach ($this->getNearestToStep($from) as $np)
        {
            if ($this->getD1($p) > $this->getD1($np))
            {
                $p = $np;
            }
        }
        return $p;
    }

    public function setLine($y, $line) {
        for ($i = 0; $i < strlen($line); $i++)
        {
            $this->set(new Point($i, $y), $line[$i]);
        }
    }

    public function width()
    {
        return count($this->data);
    }

    public function height()
    {
        return count($this->data[0]);
    }

    private function randP()
    {
        return (!rand(0, 3) ? '#':'.');
    }

    public function __construct($width, $height, $rand = false) {
        for($i = 0; $i < $width; $i ++) {
            for ($j = 0; $j < $height; $j++) {
                $c = $rand ? $this->randP() : '?';
                $this->set(new Point($i, $j), $c);
            }
        }
    }

    private function resetD1()
    {
        for($i = 0; $i < $this->width(); $i ++) {
            $this->d1 [$i] = array_fill ( 0, $this->height(), -1 );
        }
    }

    public function set(Point $p, $c) {
        $this->data [$p->x] [$p->y] = $c;
        if ($c == "C")
        {
            $this->pointC = $p;
        }
        if ($c == "T")
        {
            $this->pointT = $p;
        }
    }

    public function setD1(Point $p, $c) {
        $this->d1 [$p->x] [$p->y] = $c;
    }

    public function get(Point $p)
    {
        return $this->data[$p->x][$p->y];
    }

    public function getD1(Point $p)
    {
        return $this->d1[$p->x][$p->y];
    }

    public function show()
    {
        for ($j = 0; $j < $this->height(); $j++)
        {
            for ($i = 0; $i < $this->width(); $i++)
            {
                echo $this->get(new Point($i, $j));
            }
            echo "\n";
        }
    }

    public function showD()
    {
        echo "<table border=1 cellpadding=5 cellspacing=0>";
        for ($j = 0; $j < $this->height(); $j++)
        {
            echo "<tr>";
            for ($i = 0; $i < $this->width(); $i++)
            {
                echo "<td>".$this->getD1(new Point($i, $j));
            }
        }
        echo "</table>";
    }

    private function fillCell($p)
    {
        $min = -1;
        foreach ($this->getNearestToStep($p) as $np)
        {
            $d = $this->getD1($np);
            if ($d > -1 && ($d < $min || $min == -1))
            {
                $min = $d;
            }
        }

        $this->setD1($p, $min + 1);

        foreach ($this->getNearestToStep($p) as $np)
        {
            if ($this->getD1($np) == -1)
            {
                if (!in_array($np, $this->queue)) {
                    array_push($this->queue, $np);
                }
            }
        }

        if (count($this->queue)) {
            $this->fillCell(array_shift($this->queue));
        }
    }

    /**
     * Nearest visible cells to step
     * @param Point $p
     * @return Points[]
     */
    private function getNearestToStep(Point $p)
    {
        $points = array();
        foreach ($p->neighb() as $dp)
        {
            if ($this->canStep($dp))
            {
                $points[] = $dp;
            }
        }
        return $points;
    }

    /**
     * Get Neighbourhood invisible
     * @param Point $p
     * @return Point
     */
    private function getNeibQ(Point $p)
    {
        foreach ($p->neighb() as $dp)
        {
            if (!$this->isVisible($dp))
            {
                return $dp;
            }
        }
    }

    /**
     * The cell is visible and can be step here
     * @return boolean
     */
    private function canStep(Point $p)
    {
        return $this->inside($p) && !$this->isWall($p) && $this->isVisible($p);
    }

    private function isVisible(Point $p)
    {
        return $this->get($p) != '?';
    }

    private function isWall(Point $p)
    {
        return $this->get($p) == '#';
    }

    private function inside(Point $p)
    {
        return $p->insideLab($this);
    }

}

class Point
{
    public $x, $y;
    public function __construct($x, $y)
    {
        $this->x = $x;
        $this->y = $y;
    }

    public function pointAt($dx, $dy)
    {
        return new self($this->x + $dx, $this->y + $dy);
    }

    public function insideLab(Labirint $lab)
    {
        return ($this->x >= 0 && $this->y >= 0 && $this->x < $lab->width() && $this->y < $lab->height());
    }

    public function show()
    {
        echo "($this->x, $this->y)\n";
    }

    public function go(Point $next)
    {
        if ($next->x > $this->x) return "RIGHT";
        if ($next->x < $this->x) return "LEFT";
        if ($next->y < $this->y) return "UP";
        if ($next->y > $this->y) return "DOWN";
    }

    /**
     * return four neighbourhoods
     */
    public function neighb()
    {
        $points = array();
        for ($i=0; $i<4; $i++)
        {
            $dx = (($i+1)%2) * (1-$i);
            $dy = ($i%2) * ($i-2);
            $dp = $this->pointAt($dx, $dy);
            $points[] = $dp;
        }
        return $points;
    }

    public function nearTo(Point $point)
    {
        return abs($this->x - $point->x)<=1 && abs($this->y - $point->y)<=1;
    }
}