# README #

These are some solutions for codingame.com tasks.

See my profile: [http://www.codingame.com/profile/5b9b8740443b06fe0d097fe0a6ccf187933616](http://www.codingame.com/profile/5b9b8740443b06fe0d097fe0a6ccf187933616)

1. [Labirinth](https://bitbucket.org/rastaturin/codingame/src/master/src/my/projects/Labirint/) - PHP
2. [CGX Parser](https://bitbucket.org/rastaturin/codingame/src/master/src/my/projects/CGX/) - Java
3. [Bender](https://bitbucket.org/rastaturin/codingame/src/master/src/my/projects/Bender/) - PHP